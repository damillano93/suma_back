# Cuso de la imagen de nodejs
FROM node
# Copiar el archivo package.json
RUN mkdir -p /usr/src/app
# Instalar modulos npm
WORKDIR /usr/src/app
# Instalar forever para ejecutar en segundo plano
COPY . .
# Instala mocha para ejecutar los test
RUN npm install
# Copiar los archivos de codigo fuente
EXPOSE 3000
# ejecutar comando para iniciar la API
CMD ["node", "app.js"]