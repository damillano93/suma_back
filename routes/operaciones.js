//llamado a librerias
var express = require('express');
var router = express.Router();

//llamado a los controllers
var operaciones_controller = require('../controllers/operaciones');


//creacion ruta POST suma
router.post('/suma', operaciones_controller.suma);
//creacion ruta POST resta
router.post('/resta', operaciones_controller.resta);
//creacion ruta get /
router.get('/',operaciones_controller.status);

module.exports = router;