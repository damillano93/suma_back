// app.js
//agregar librerias
var express = require('express');
var bodyParser = require('body-parser');
logger = require('morgan')
//importar rutas
var operaciones = require('./routes/operaciones'); 
var app = express();
app.use(logger('dev'))
//agrega CORS para tener accesso desde otras aplicaciones
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
  });


//uso de JSON
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use('/operaciones', operaciones);
//puerto del servicio
var port = 3000;
//inicializacion del servicio
app.listen(port, () => {
    console.log('Servidor OK en puerto: ' + port);
});
