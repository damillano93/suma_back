var superagent = require('superagent')
var expect = require('expect.js')

describe('express rest api server', function () {
  var id
  // GET /operaciones
  it('probar status API', function (done) {
    superagent.get('http://localhost:3000/operaciones/')
      .end(function (e, res) {

        expect(e).to.eql(null)
        expect(typeof res.body).to.eql('object')
        expect(res.body.status).to.eql('Okey')
        done()
      })
  })
  // POST /operaciones/suma {"datoa":"1","datob":"1"}
  it('operacion suma {"datoa":"1","datob":"1"} ', function (done) {
    superagent.post('http://localhost:3000/operaciones/suma/')
      .send({
        datoa: '1'
        , datob: '1'
      })
      .end(function (e, res) {
        expect(e).to.eql(null)
        expect(res.body.resultado).to.eql(2)
        done()
      })
  })
  // POST /operaciones/suma {"daroa":"1.5","datob":"1.6"}
  it('operacion suma decimal  {"datoa":"1.5","datob":"1.6"}', function (done) {
    superagent.post('http://localhost:3000/operaciones/suma/')
      .send({
        datoa: '1.5'
        , datob: '1.6'
      })
      .end(function (e, res) {
        expect(e).to.eql(null)
        expect(res.body.resultado).to.eql(3.1)
        done()
      })
  })
// POST /operaciones/suma {"datoa":"1","datob":"a"}
  it('operacion suma {"datoa":"hola","datob":"1"}', function (done) {
    superagent.post('http://localhost:3000/operaciones/suma/')
      .send({
        datoa: 'hola'
        , datob: '1'
      })
      .end(function (e, res) {
        expect(e).to.eql(null)
        expect(res.body.status).to.eql("Err")
        expect(res.body.resultado).to.eql("debe introducir dos numeros validos")
        done()
      })
  })
  // POST /operaciones/suma {"daroa":"a","datob":"1"}
  it('operacion suma {"datoa":"1","datob":"hola"}', function (done) {
    superagent.post('http://localhost:3000/operaciones/suma/')
      .send({
        datoa: '1'
        , datob: 'hola'
      })
      .end(function (e, res) {
        expect(e).to.eql(null)
        expect(res.body.status).to.eql("Err")
        expect(res.body.resultado).to.eql("debe introducir dos numeros validos")
        done()
      })
  })
  // POST /operaciones/suma {"daroa":"a","datob":"a"}
  it('operacion suma {"datoa":"hola","datob":"hola"}', function (done) {
    superagent.post('http://localhost:3000/operaciones/suma/')
      .send({
        datoa: 'hola'
        , datob: 'hola'
      })
      .end(function (e, res) {
        expect(e).to.eql(null)
        expect(res.body.status).to.eql("Err")
        expect(res.body.resultado).to.eql("debe introducir dos numeros validos")
        done()
      })
  })
  // POST /operaciones/suma {"adatoa":"1","datob":"1"}
  it('operacion suma datoa faltante {"adatoa":"1","datob":"1"}', function (done) {
    superagent.post('http://localhost:3000/operaciones/suma/')
      .send({
        adatoa: '1'
        , datob: '1'
      })
      .end(function (e, res) {
        expect(e).to.eql(null)
        expect(res.body.status).to.eql("Err")
        expect(res.body.resultado).to.eql("debe introducir dos numeros validos")
        done()
      })
  })
  // POST /operaciones/suma {"datoa":"1","adatob":"1"}
  it('operacion suma datob faltante {"datoa":"1","adatob":"1"}', function (done) {
    superagent.post('http://localhost:3000/operaciones/suma/')
      .send({
        datoa: '1'
        , adatob: '1'
      })
      .end(function (e, res) {
        expect(e).to.eql(null)
        expect(res.body.status).to.eql("Err")
        expect(res.body.resultado).to.eql("debe introducir dos numeros validos")
        done()
      })
  })
  // POST /operaciones/suma {"adatoa":"1","adatob":"1"}
  it('operacion suma datoa y datob faltante {"datoa1":"1","adatob1":"1"}', function (done) {
    superagent.post('http://localhost:3000/operaciones/suma/')
      .send({
        datoa1: '1'
        , datob1: '1'
      })
      .end(function (e, res) {
        expect(e).to.eql(null)
        expect(res.body.status).to.eql("Err")
        expect(res.body.resultado).to.eql("debe introducir dos numeros validos")
        done()
      })
  })

    // POST /operaciones/resta {"datoa":"8","datob":"5"}
    it('operacion resta {"datoa":"8","datob":"} ', function (done) {
      superagent.post('http://localhost:3000/operaciones/resta/')
        .send({
          datoa: '8'
          , datob: '3'
        })
        .end(function (e, res) {
          expect(e).to.eql(null)
          expect(res.body.resultado).to.eql(5)
          done()
        })
    })
    // POST /operaciones/resta {"daroa":"3.5","datob":"1.3"}
    it('operacion resta decimal  {"datoa":"3.5","datob":"1.3"}', function (done) {
      superagent.post('http://localhost:3000/operaciones/resta/')
        .send({
          datoa: '3.5'
          , datob: '1.3'
        })
        .end(function (e, res) {
          expect(e).to.eql(null)
          expect(res.body.resultado).to.eql(2.2)
          done()
        })
    })
  // POST /operaciones/resta {"datoa":"1","datob":"a"}
    it('operacion resta {"datoa":"hola","datob":"1"}', function (done) {
      superagent.post('http://localhost:3000/operaciones/resta/')
        .send({
          datoa: 'hola'
          , datob: '1'
        })
        .end(function (e, res) {
          expect(e).to.eql(null)
          expect(res.body.status).to.eql("Err")
          expect(res.body.resultado).to.eql("debe introducir dos numeros validos")
          done()
        })
    })
    // POST /operaciones/resta {"daroa":"a","datob":"1"}
    it('operacion resta {"datoa":"1","datob":"hola"}', function (done) {
      superagent.post('http://localhost:3000/operaciones/resta/')
        .send({
          datoa: '1'
          , datob: 'hola'
        })
        .end(function (e, res) {
          expect(e).to.eql(null)
          expect(res.body.status).to.eql("Err")
          expect(res.body.resultado).to.eql("debe introducir dos numeros validos")
          done()
        })
    })
    // POST /operaciones/resta {"daroa":"a","datob":"a"}
    it('operacion resta {"datoa":"hola","datob":"hola"}', function (done) {
      superagent.post('http://localhost:3000/operaciones/resta/')
        .send({
          datoa: 'hola'
          , datob: 'hola'
        })
        .end(function (e, res) {
          expect(e).to.eql(null)
          expect(res.body.status).to.eql("Err")
          expect(res.body.resultado).to.eql("debe introducir dos numeros validos")
          done()
        })
    })
    // POST /operaciones/resta {"adatoa":"1","datob":"1"}
    it('operacion resta datoa faltante {"adatoa":"1","datob":"1"}', function (done) {
      superagent.post('http://localhost:3000/operaciones/resta/')
        .send({
          adatoa: '1'
          , datob: '1'
        })
        .end(function (e, res) {
          expect(e).to.eql(null)
          expect(res.body.status).to.eql("Err")
          expect(res.body.resultado).to.eql("debe introducir dos numeros validos")
          done()
        })
    })
    // POST /operaciones/resta {"datoa":"1","adatob":"1"}
    it('operacion resta datob faltante {"datoa":"1","adatob":"1"}', function (done) {
      superagent.post('http://localhost:3000/operaciones/resta/')
        .send({
          datoa: '1'
          , adatob: '1'
        })
        .end(function (e, res) {
          expect(e).to.eql(null)
          expect(res.body.status).to.eql("Err")
          expect(res.body.resultado).to.eql("debe introducir dos numeros validos")
          done()
        })
    })
    // POST /operaciones/resta {"adatoa":"1","adatob":"1"}
    it('operacion resta datoa y datob faltante {"datoa1":"1","adatob1":"1"}', function (done) {
      superagent.post('http://localhost:3000/operaciones/resta/')
        .send({
          datoa1: '1'
          , datob1: '1'
        })
        .end(function (e, res) {
          expect(e).to.eql(null)
          expect(res.body.status).to.eql("Err")
          expect(res.body.resultado).to.eql("debe introducir dos numeros validos")
          done()
        })
    })

})
